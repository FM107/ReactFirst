import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import globalStore from "../../store/globalStore";
import { Button, message, Modal, Table } from "antd";
import api from "../../api";
const { confirm } = Modal;
interface dataType {
  auditState: 0 | 1 | 2;
  author: string;
  categoryId: number;
  content: string;
  createTime: number;
  id: number;
  publishState: 0 | 1 | 2;
  publishTime: number;
  region: string;
  roleId: number;
  star: number;
  title: string;
  view: number;
  category: slectType;
}
interface slectType {
  id: number;
  title: string;
  value: string;
}
type DataArray = Array<dataType>;
const {
  reqTapeOut,
  reqReleased,
  reqUnpublished,
  reqPublish,
  reqOutLineNews,
  reqDelNewsOn,
  reqRestLineNews,
} = api;

export default function index(props: { type: 1 | 2 | 3 }) {
  const rouleId: number = Number(localStorage.getItem("token"));
  const userName: string = localStorage.getItem("userName") || "";
  const [data, useData] = useState<DataArray>([]);
  const getData = async () => {
    let result: DataArray = [];
    switch (type) {
      case 1:
        result = (await reqUnpublished()) as unknown as DataArray;
        break;
      case 2:
        result = (await reqReleased()) as unknown as DataArray;
        break;
      case 3:
        result = (await reqTapeOut()) as unknown as DataArray;
        break;
    }
    if (rouleId) {
      if (rouleId === 2) {
        result = result.filter((item) => {
          if (item.roleId > rouleId) return true;
          if (item.author === userName) return true;
        });
      } else if (rouleId === 3) {
        result = result.filter((item) => {
          if (item.author === userName) return true;
        });
      }
      {
      }
    }
    useData(result);
  };
  const columns = [
    {
      title: "新闻标题",
      dataIndex: "title",
      key: "title",
      render(value: string, item: dataType) {
        return (
          <Link to={`/main/news-manage/preview/${item.id}`} children={value} />
        );
      },
    },
    {
      title: "作者",
      dataIndex: "author",
      key: "author",
    },
    {
      title: "新闻分类",
      dataIndex: "category",
      key: "category",
      render(_: any, item: dataType) {
        return item.category.title;
      },
    },
    {
      title: "操作",
      render(item: dataType) {
        const buttonArr = [
          ,
          <Button
            children="发布"
            type="primary"
            style={{ margin: "0 10px" }}
            onClick={() => {
              confirm({
                icon: "",
                title: "确定发布吗",
                okText: "确定",
                cancelText: "取消",
                onOk: async () => {
                  await reqPublish(item.id);
                  await getData();
                  message.success("发布成功");
                },
              });
            }}
          />,
          <Button
            children="下线"
            type="primary"
            onClick={() => {
              confirm({
                title: "此新闻即将下线，确定吗",
                content: "请慎重下线",
                okText: "确定",
                cancelText: "取消",
                async onOk() {
                  await reqOutLineNews(item.id);
                  await getData();
                  message.success("已下线");
                },
              });
            }}
          />,
          <>
            <Button
              children="删除"
              danger
              style={{ margin: "0 10px" }}
              onClick={() => {
                confirm({
                  title: "确定删除吗",
                  content: "警告！此新闻会永久删除！",
                  okText: "确定",
                  cancelText: "取消",
                  onOk: async () => {
                    await reqDelNewsOn(item.id);
                    await getData();
                    reqDelNewsOn;
                    message.success("已删除");
                  },
                });
              }}
            />
            <Button
              children="重新上线"
              type="primary"
              onClick={() => {
                confirm({
                  icon: "",
                  title: "确定重新上线吗",
                  content: "此条新闻将会重新回复已发布的状态",
                  okText: "确定",
                  cancelText: "取消",
                  onOk: async () => {
                    await reqRestLineNews(item.id);
                    await getData();
                    reqDelNewsOn;
                    message.success("重新发布成功");
                  },
                });
              }}
            />
          </>,
        ];
        return buttonArr[type];
      },
    },
  ];
  const { type } = props;
  useEffect(() => {
    globalStore.data.titel =
      type === 1 ? "待发布" : type === 2 ? "已发布" : "已下线";
    useData([]);
    getData();
  }, [type]);
  return (
    <Table
      dataSource={data}
      columns={columns}
      rowKey="id"
      pagination={{ pageSize: 5 }}
    />
  );
}
