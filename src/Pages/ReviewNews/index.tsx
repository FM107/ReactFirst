import { useState, useEffect } from "react";
import { useNavigate, Link } from "react-router-dom";
import { Button, Modal, Tag, message, Table } from "antd";
import globalStore from "../../store/globalStore";
import api from "../../api";
const { reqReviewNews, reqAgreedReview, reqRejectNews } = api;
const { confirm } = Modal;
interface dataType {
  auditState: 0 | 1 | 2;
  author: string;
  categoryId: number;
  content: string;
  createTime: number;
  id: number;
  publishState: 0 | 1 | 2;
  publishTime: number;
  region: string;
  roleId: number;
  star: number;
  title: string;
  view: number;
  category: slectType;
}
interface slectType {
  id: number;
  title: string;
  value: string;
}

type arrData = Array<dataType>;

export default function index() {
  const [data, useData] = useState<arrData>([]);
  const nav = useNavigate();
  const getReviewData = async () => {
    const reuslt = (await reqReviewNews()) as unknown as arrData;
    const region = localStorage.getItem("userRegion");
    const filters = reuslt.filter((item) => {
      if (region) {
        return item.region === region;
      }
      return true;
    });
    useData(filters);
  };
  const columns = [
    {
      title: "新闻标题",
      dataIndex: "title",
      key: "title",
      render(value: string, item: dataType) {
        return (
          <Link to={`/main/news-manage/preview/${item.id}`} children={value} />
        );
      },
    },
    {
      title: "作者",
      dataIndex: "author",
      key: "author",
    },
    {
      title: "新闻分类",
      dataIndex: "category",
      key: "category",
      render(_: any, item: dataType) {
        return item.category.title;
      },
    },
    {
      title: "操作",
      render(item: dataType) {
        return (
          <>
            <Button
              children="通过"
              type="primary"
              style={{ margin: "0 10px" }}
              onClick={() => {
                confirm({
                  title: "警示",
                  content: "请确认此新闻符合审核标准",
                  okText: "确定",
                  cancelText: "取消",
                  onOk: async () => {
                    await reqAgreedReview(item.id);
                    await getReviewData();
                    message.success("已通过");
                  },
                });
              }}
            />

            <Button
              children="驳回"
              danger
              onClick={() => {
                confirm({
                  icon: "",
                  title: "确定要驳回吗",
                  cancelText: "取消",
                  okText: "确定",
                  onOk: async () => {
                    await reqRejectNews(item.id);
                    await getReviewData();
                    message.success("已驳回");
                  },
                });
              }}
            />
          </>
        );
      },
    },
  ];

  useEffect(() => {
    globalStore.data.titel = "审核列表";
    getReviewData();
  }, []);
  return <Table dataSource={data} columns={columns} rowKey="id" />;
}
