import { creatProxy, ProxyTypes } from "../../rstore-react-ts";
import globalStore from "../../store/globalStore";
const Test = function Test(props: ProxyTypes) {
  const { bindData, data } = props.rStatus;
  bindData(["testData"], "global");
  console.log("更新了", data);
  return (
    <div>
      <button
        onClick={() => {
          data.global.testData.name = "小河";
          console.log("触发了");
          console.log(data.global);
        }}
      >
        进行深层次的响应式触发
      </button>
      <button
        onClick={() => {
          data.global.testArr.push("123123123");
        }}
      >
        进行深层此数组方法
      </button>
      <h1>{data.global.testData.name}</h1>
      <h2>{data.global.testArr}</h2>
    </div>
  );
};

export default creatProxy(Test, ["global"]);
