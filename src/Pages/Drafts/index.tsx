import { Button, Table, Modal, message } from "antd";
import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import request from "../../api";
import globalStore from "../../store/globalStore";
const { reqDrafts, reqCatrGread, reqChnageDrafts, reqDelNews } = request;
const { confirm } = Modal;

interface tableDataType {
  auditState: 0 | 1 | 2;
  author: string;
  categoryId: number;
  content: string;
  createTime: number;
  id: number;
  publishState: 0 | 1 | 2;
  publishTime: number;
  region: string;
  roleId: number;
  star: number;
  title: string;
  view: number;
}

interface slectType {
  id: number;
  title: string;
  value: string;
}
type slectArr = Array<slectType>;

type arrTableDataType = Array<tableDataType>;

export default function index() {
  const nav = useNavigate();
  const getTableData = async () => {
    const result = (await reqDrafts()) as unknown as arrTableDataType;
    useTableData(result);
  };
  const getSlectData = async () => {
    const result = (await reqCatrGread()) as unknown as slectArr;
    useSlectdata(result);
  };
  useEffect(() => {
    globalStore.data.titel = "草稿箱";
  }, []);
  const [tableData, useTableData] = useState<arrTableDataType>([]);
  const [slectData, useSlectdata] = useState<slectArr>([]);
  const mapping = (value: number) => {
    return slectData.find((item) => item.id === value)?.title;
  };
  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "新闻标题",
      dataIndex: "title",
      key: "title",
      render: (value: string, item: tableDataType) => {
        return (
          <Link to={`/main/news-manage/preview/${item.id}`} children={value} />
        );
      },
    },
    {
      title: "作者",
      dataIndex: "author",
      key: "author",
    },
    {
      title: "新闻分类",
      dataIndex: "categoryId",
      key: "categoryId",
      render: (item: number) => {
        return mapping(item);
      },
    },
    {
      title: "操作",
      render: (_: any, item: tableDataType) => (
        <>
          <Button
            danger
            onClick={() => {
              confirm({
                title: "您确定要删除吗",
                content: "请确定您十分确认要删除此条新闻,而不是误操作",
                okText: "确定",
                cancelText: "取消",
                async onOk() {
                  await reqDelNews(item.id);
                  await getTableData();
                  message.success("已删除");
                },
              });
            }}
          >
            删除
          </Button>
          <Button
            type="default"
            style={{ margin: "0 10px" }}
            onClick={() => {
              nav(`/main/news-manage/update/${item.id}`);
            }}
          >
            编辑
          </Button>
          <Button
            type="primary"
            onClick={() => {
              confirm({
                title: "确定提交审核",
                content: "在提交审核之前，请您确定您的文章是否准确无误",
                okText: "确定",
                icon: "",
                cancelText: "取消",
                async onOk() {
                  item.auditState = 1;
                  await reqChnageDrafts(item.id, item);
                  await getTableData();
                  message.success("提交成功");
                },
              });
            }}
          >
            提交审核
          </Button>
        </>
      ),
    },
  ];
  useEffect(() => {
    getTableData();
    getSlectData();
  }, []);
  return (
    <Table
      dataSource={tableData}
      columns={columns}
      pagination={{ pageSize: 5 }}
      rowKey="id"
    />
  );
}
