import { PageHeader, Descriptions } from "antd";
import { useEffect, useRef, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import moment from "moment";

import api from "../../api";
const { reqNewsDetial, reqCatrGread } = api;
interface dataType {
  auditState: 0 | 1 | 2;
  author: string;
  categoryId: number;
  content: string;
  createTime: number;
  id: number;
  publishState: 0 | 1 | 2;
  publishTime: number;
  region: string;
  roleId: number;
  star: number;
  title: string;
  view: number;
}
interface slectType {
  id: number;
  title: string;
  value: string;
}
type slectArr = Array<slectType>;
export default function index() {
  const { id } = useParams();
  const getSlectData = async () => {
    const result = (await reqCatrGread()) as unknown as slectArr;
    useSlectdata(result);
  };

  const mapping = (value: number) => {
    return slectData.find((item) => item.id === value)?.title;
  };
  const wacthStuts = (id: number) => {
    const arr = ["未审核", "待发布", "已发布", "已下线"];
    return arr[id];
  };
  const pushStuts = (id: number) => {
    const arr = ["草稿箱", "待审核", "通过审核", "审核失败"];
    return arr[id];
  };
  const [slectData, useSlectdata] = useState<slectArr>([]);
  const [data, useData] = useState<dataType | undefined>();
  const divref = useRef<HTMLDivElement>(null);
  const getNewsData = async () => {
    const result = (await reqNewsDetial(Number(id))) as unknown as dataType;
    useData(result);
  };
  useEffect(() => {
    if (divref.current !== null && data) {
      divref.current.innerHTML = data.content;
    }
  }, [data]);
  useEffect(() => {
    getNewsData();
    getSlectData();
  }, []);
  const navigate = useNavigate();
  return data ? (
    <>
      <PageHeader
        ghost={false}
        onBack={() => navigate(-1)}
        title={data.title}
        subTitle={mapping(data.categoryId)}
      >
        <Descriptions size="small" column={3}>
          <Descriptions.Item label="创作者">{data.author}</Descriptions.Item>
          <Descriptions.Item label="创建时间">
            {moment(data.createTime).format("YYYY MM DD, h:mm:ss")}
          </Descriptions.Item>
          <Descriptions.Item label="发布时间">
            {data.publishTime
              ? moment(data.publishTime).format("YYYY MM DD, h:mm:ss")
              : "-"}
          </Descriptions.Item>
          <Descriptions.Item label="区域">{data.region}</Descriptions.Item>
          <Descriptions.Item label="审核状态">
            <p style={{ color: "red" }}>{wacthStuts(data.publishState)}</p>
          </Descriptions.Item>
          <Descriptions.Item label="发布状态">
            <p style={{ color: "red" }}>{pushStuts(data.auditState)}</p>
          </Descriptions.Item>
          <Descriptions.Item label="访问数量">
            <p style={{ color: "green" }}> {data.view}</p>
          </Descriptions.Item>
          <Descriptions.Item label="点赞数量">
            <p style={{ color: "green" }}> {data.star}</p>
          </Descriptions.Item>
          <Descriptions.Item label="评论数量">
            <p style={{ color: "green" }}> {0}</p>
          </Descriptions.Item>
        </Descriptions>
      </PageHeader>
      <h2 style={{ textAlign: "center", fontWeight: 700, marginTop: "30px" }}>
        预览
      </h2>
      <div
        ref={divref}
        style={{
          width: "100%",
          height: "60%",
          overflow: "auto",
          marginTop: "10px",
          backgroundColor: "white",
        }}
      ></div>
    </>
  ) : null;
}
