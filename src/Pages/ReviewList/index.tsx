import { Button, Table, Tag, Modal, message } from "antd";
import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import api from "../../api";
import globalStore from "../../store/globalStore";
const { reqReview, reqChnageDrafts } = api;
const { confirm } = Modal;
interface dataType {
  auditState: 0 | 1 | 2;
  author: string;
  categoryId: number;
  content: string;
  createTime: number;
  id: number;
  publishState: 0 | 1 | 2;
  publishTime: number;
  region: string;
  roleId: number;
  star: number;
  title: string;
  view: number;
  category: slectType;
}
interface slectType {
  id: number;
  title: string;
  value: string;
}

type arrData = Array<dataType>;

export default function index() {
  const [data, useData] = useState<arrData>([]);
  const nav = useNavigate();
  const getReviewData = async () => {
    const reuslt = (await reqReview()) as unknown as arrData;
    const userName = localStorage.getItem("userName");
    const filters = reuslt.filter((item) => {
      if (userName) {
        return item.author === userName;
      }
      return true;
    });
    useData(filters);
  };
  useEffect(() => {
    globalStore.data.titel = "审核列表";
    getReviewData();
  }, []);
  const columns = [
    {
      title: "新闻标题",
      dataIndex: "title",
      key: "title",
      render(value: string, item: dataType) {
        return (
          <Link to={`/main/news-manage/preview/${item.id}`} children={value} />
        );
      },
    },
    {
      title: "作者",
      dataIndex: "author",
      key: "author",
    },
    {
      title: "新闻分类",
      dataIndex: "category",
      key: "category",
      render(_: any, item: dataType) {
        return item.category.title;
      },
    },
    {
      title: "审核状态",
      dataIndex: "auditState",
      key: "auditState",
      render(value: number, item: dataType) {
        const stuts = [
          ,
          <Tag color="blue">审核中</Tag>,
          <Tag color="success">已通过</Tag>,
          <Tag color="error">审核失败</Tag>,
        ];
        return stuts[value];
      },
    },
    {
      title: "操作",
      render(item: dataType) {
        const status = [
          ,
          <Button
            children="撤销"
            onClick={() => {
              confirm({
                icon: "",
                title: "确认撤回吗",
                content: "撤回的新闻将储存在草稿箱中",
                okText: "确定",
                cancelText: "取消",
                onOk: async () => {
                  item.auditState = 0;
                  await reqChnageDrafts(item.id, item);
                  await getReviewData();
                  message.success("撤回成功，请在暂存箱查看被撤回的新闻");
                },
              });
            }}
          />,
          <Button
            children="发布"
            type="primary"
            onClick={() => {
              confirm({
                icon: "",
                title: "是否确定发布？",
                okText: "确定",
                cancelText: "取消",
                onOk: async () => {
                  item.publishState = 2;
                  item.publishTime = new Date().getTime();
                  await reqChnageDrafts(item.id, item);
                  await getReviewData();
                  message.success("已发布");
                },
              });
            }}
          />,
          <Button
            children="更新"
            danger
            onClick={() => {
              nav(`/main/news-manage/update/${item.id}`);
            }}
          />,
        ];
        return status[item.auditState];
      },
    },
  ];

  return <Table dataSource={data} columns={columns} rowKey="id" />;
}
