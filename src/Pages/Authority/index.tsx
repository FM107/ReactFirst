import { Table, Button, Tag, Modal, message } from "antd";
import requset from "../../api";
import globalStore from "../../store/globalStore";
import { useEffect } from "react";
import { creatProxy, ProxyTypes } from "../../rstore-react-ts";
import globalBus from "../../store/globalBus";

const refreshRouter = async () => {
  const result = (await reqSomeRoult(
    Number(localStorage.getItem("token")) || 0
  )) as unknown as any;
  globalBus.asyncChangeState("changeRouter", [result[0].rights]);
};

const {
  reqDelAuthority,
  reqDelDoubleAuth,
  reqChangeAuthority,
  reqSomeRoult,
  reqDoubleChangeAuthority,
} = requset;

const { confirm } = Modal;

function index(props: ProxyTypes) {
  const {
    bindData,
    unBindData,
    data: { global },
    actions,
  } = props.rStatus;

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "权限名称",
      dataIndex: "title",
      key: "title",
    },
    {
      title: "权限路径",
      dataIndex: "key",
      key: "key",
      render(text: any) {
        return <Tag children={"/" + text} color="blue" />;
      },
    },
    {
      title: "操作",
      render(text: any, record: any, index: any) {
        return (
          <>
            <Button
              children={
                record.pagepermisson || typeof record.pagepermisson !== "number"
                  ? "关闭"
                  : "打开"
              }
              type={record.pagepermisson ? "link" : "primary"}
              disabled={typeof record.pagepermisson !== "number"}
              style={{ margin: "0 10px" }}
              onClick={async () => {
                if (record.grade === 1) {
                  await reqChangeAuthority(
                    record.id,
                    record.pagepermisson === 1 ? 0 : 1
                  );
                } else if (record.grade === 2) {
                  await reqDoubleChangeAuthority(
                    record.id,
                    record.pagepermisson === 1 ? 0 : 1
                  );
                }
                await refreshRouter();
                await globalStore.actions.changSlectList();
              }}
            />
            <Button
              children="删除"
              danger
              onClick={() => {
                confirm({
                  title: "确认删除吗",
                  async onOk() {
                    if (record.grade === 1) {
                      await reqDelAuthority(record.id);
                    } else if (record.grade === 2) {
                      await reqDelDoubleAuth(record.id);
                    }
                    await globalStore.actions.changSlectList();
                    await globalBus.asyncChangeState(
                      "changeRouter",
                      globalStore.data.slectList
                    );
                    await refreshRouter();
                    await message.success("已删除");
                  },
                  okText: "是的",
                  cancelText: "取消",
                });
              }}
            />
          </>
        );
      },
    },
  ];
  useEffect(() => {
    actions.global.changeTitel("权限管理");
    bindData(["slectList"], "global");
    return () => {
      unBindData(["slectList"], "global");
    };
  }, []);

  return (
    <Table
      columns={columns}
      style={{ overflow: "auto", height: "100%" }}
      dataSource={global.slectList}
      pagination={{ pageSize: 5 }}
    />
  );
}

export default creatProxy(index, ["global"]);
