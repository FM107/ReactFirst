import { creatProxy, ProxyTypes } from "../../rstore-react-ts";
import { Table, Button, Modal, message } from "antd";
import request from "../../api";
import { useState, useEffect } from "react";
const { reqRoule, reqDelRoule, reqChangeRoule } = request;
const { confirm } = Modal;
type listDataType = Array<{
  id: number;
  roleName: string;
  roleType: number;
  rights: Array<string>;
}>;
import { Tree } from "antd";

const Role = (props: ProxyTypes) => {
  const {
    actions,
    data: { global },
  } = props.rStatus;

  const getListData = async () => {
    const result = (await reqRoule()) as unknown as listDataType;
    useListData(result);
  };
  const [listData, useListData] = useState<listDataType>([]);
  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      keys: "id",
    },
    {
      title: "角色名称",
      dataIndex: "roleName",
      keys: "roleName",
    },
    {
      title: "操作",
      keys: "button",
      render: (item: any) => {
        return (
          <>
            <Button
              children="删除"
              style={{ margin: "0 10px" }}
              danger
              onClick={() => {
                confirm({
                  type: "error",
                  title: "您正在尝试删除",
                  okText: "确定",
                  cancelText: "取消",
                  async onOk() {
                    await reqDelRoule(item.id);
                    await getListData();
                    message.success("删除成功");
                  },
                });
              }}
            />
            <Button
              children="编辑"
              type="primary"
              onClick={() => {
                confirm({
                  title: "权限节点树",
                  type: "info",
                  icon: "",
                  content: (
                    <Tree
                      checkable
                      treeData={global.slectList}
                      fieldNames={{
                        key: "title",
                      }}
                      checkStrictly
                      defaultCheckedKeys={item.rights}
                      onCheck={async (e) => {
                        await reqChangeRoule(item.id, e as string[]);
                        await getListData();
                      }}
                    />
                  ),
                });
              }}
            />
          </>
        );
      },
    },
  ];
  useEffect(() => {
    getListData();
    actions.global.changeTitel("角色管理");
  }, []);
  return <Table dataSource={listData} columns={columns} rowKey="id" />;
};

export default creatProxy(Role, ["global"]);
