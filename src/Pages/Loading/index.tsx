import React from "react";
import { Spin } from "antd";

export default function index() {
  return (
    <div
      style={{
        width: "100%",
        height: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Spin />
      <h2>加载中</h2>
    </div>
  );
}
