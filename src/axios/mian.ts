import axios from "axios";
const request = axios.create({
  baseURL: "http://localhost:3004",
});

request.interceptors.request.use((config) => {
  return config;
});

request.interceptors.response.use((res) => {
  if (res.status >= 200 && res.status < 300) {
    return res.data;
  } else {
    return Promise.reject(new Error(res.statusText));
  }
});

export default request;
