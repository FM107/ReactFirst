import axios from "axios";
const instance = axios.create({
  baseURL: "saer",
});

instance.interceptors.request.use((config) => {
  return config;
});

instance.interceptors.response.use((resopns) => {
  if (resopns.status === 200) {
    return resopns.data;
  } else {
    return Promise.reject(new Error("网路请求出错请检查"));
  }
});

export default instance;
