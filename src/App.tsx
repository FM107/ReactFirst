import "./format.module.less";

import app_less from "./App.module.less";
import Router from "./router/IndexRouter";
import Breads from "./Components/Breads";
import Sidebar from "./Components/Sidebar";
import { useLocation } from "react-router-dom";
import Test from "./Pages/Test";
export default function App() {
  const loaction = useLocation();
  if (
    loaction.pathname === "/login" ||
    loaction.pathname === "/news" ||
    location.pathname.indexOf("/preview/") !== -1
  ) {
    return (
      <>
        <Test />
        <Router />
      </>
    );
  }
  return (
    <div className={app_less.sumBox}>
      <Sidebar></Sidebar>
      <div
        className={app_less.main}
        style={{ height: window.innerHeight + "px" }}
      >
        <Breads></Breads>
        <Router />
      </div>
    </div>
  );
}
