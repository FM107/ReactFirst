import { creatRStatus } from "../rstore-react-ts";
import { ReactElement } from "react";
import request from "../api";
const { reqRight } = request;
type dynamicMenuFunValueType = Array<{
  title: string;
  key: string;
  icons?: ReactElement;
  pagepermisson?: 1;
  children?: dynamicMenuFunValueType;
}>;
interface data {
  titel: string;
  userName: string;
  slectList: dynamicMenuFunValueType;
  roules: Array<string>;
  userRegion: string;
  testData: {
    name: string;
  };
  testArr: Array<string>;
}
export default creatRStatus<data>({
  data: {
    titel: "首页",
    userName: localStorage.getItem("userName") || "",
    slectList: [],
    roules: [],
    userRegion: localStorage.getItem("userRegion") || "",
    testData: {
      name: "小王",
    },
    testArr: ["123123123"],
  },
  name: "global",
  actions(status) {
    const changeTitel = (value: string) => {
      if (value) {
        status.data.titel = value;
      }
    };
    const changeUserName = (value: string) => {
      if (value) {
        localStorage.setItem("userName", value);
        status.data.userName = value;
      }
    };
    const changSlectList = async () => {
      const result = (await reqRight()) as unknown;
      (result as dynamicMenuFunValueType).forEach((item) => {
        if (item.children && item.children.length === 0) {
          delete item.children;
        }
      });
      status.data.slectList = result as dynamicMenuFunValueType;
    };
    return { changeTitel, changeUserName, changSlectList };
  },
});
