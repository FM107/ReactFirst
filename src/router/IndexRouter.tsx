import { Navigate, useRoutes } from "react-router-dom";
import Authority from "../Pages/Authority";
import Login from "../Pages/Login";
import IsLogin from "../guardComponents/IsLogin";
import Err404 from "../Pages/Err404";
import Role from "../Pages/Role";
import Users from "../Pages/Users";
import Home from "../Pages/Home";
import requset from "../api";
import Drafts from "../Pages/Drafts";
const { reqSomeRoult } = requset;
import Loading from "../Pages/Loading";
import { ReactElement, useState } from "react";
import globalBus from "../store/globalBus";
import globalStore from "../store/globalStore";
import { creatProxy, ProxyTypes } from "../rstore-react-ts";
import NewsAdd from "../Pages/NewsAdd";
import NewsDetial from "../Pages/NewsDetial";
import ReviewList from "../Pages/ReviewList";
import ReviewNews from "../Pages/ReviewNews";
import Category from "../Pages/Category";
import Universal from "../Pages/Universal";
import Tourist from "../Pages/Tourist";
import Preview from "../Pages/Preview";
type ListType = {
  name?: string;
  path: string;
  element: ReactElement;
  children?: Array<ListType>;
};

type routerType = Array<ListType>;

const routerOptions: routerType = [
  {
    path: "/login",
    element: <Login />,
  },
  {
    path: "/",
    element: <Navigate to="/main" />,
  },
  {
    path: "/news",
    element: <Tourist />,
  },
  {
    path: "/preview/:id",
    element: <Preview />,
  },
  {
    path: "/main",
    element: <IsLogin />,
    name: "meta",
    children: [
      {
        name: "metaLoding",
        path: "*",
        element: <Loading />,
      },
    ],
  },
  {
    path: "*",
    element: <Err404 />,
  },
];

const roleRouter: routerType = [
  {
    path: "/main",
    element: <IsLogin />,
    children: [
      {
        path: "home",
        name: "首页",
        element: <Home />,
      },
      {
        path: "right-manage/right/list",
        name: "权限管理",
        element: <Authority />,
      },
      {
        path: "right-manage/role/list",
        name: "角色列表",
        element: <Role />,
      },
      {
        path: "user-manage/list",
        name: "用户列表",
        element: <Users />,
      },
      {
        path: "news-manage/add",
        name: "撰写新闻",
        element: <NewsAdd />,
      },
      {
        path: "news-manage/draft",
        name: "草稿箱",
        element: <Drafts />,
      },
      {
        path: "news-manage/preview/:id",
        name: "新闻预览",
        element: <NewsDetial />,
      },
      {
        path: "news-manage/update/:id",
        name: "新闻更新",
        element: <NewsAdd />,
      },
      {
        path: "audit-manage/list",
        name: "审核列表",
        element: <ReviewList />,
      },
      {
        path: "audit-manage/audit",
        name: "审核新闻",
        element: <ReviewNews />,
      },
      {
        path: "news-manage/category",
        name: "新闻分类",
        element: <Category />,
      },
      {
        path: "publish-manage/unpublished",
        name: "待发布",
        element: <Universal type={1} />,
      },
      {
        path: "publish-manage/published",
        name: "已发布",
        element: <Universal type={2} />,
      },
      {
        path: "publish-manage/sunset",
        name: "已下线",
        element: <Universal type={3} />,
      },
      {
        path: "/main/*",
        element: <Err404 />,
      },
    ],
  },
];

const router = (props: ProxyTypes) => {
  const { bindData } = props.rStatus;
  const [data, useData] = useState<routerType>(routerOptions);
  const { slectList } = globalStore.data;
  const filterOpen = (value: string, arr: typeof slectList): boolean => {
    if (value) {
      return arr.some((item) => {
        if (
          (item.title === value && item.pagepermisson === 1) ||
          item.pagepermisson === undefined
        )
          return true;
        else if (item.children && item.children.length > 0) {
          return filterOpen(value, item.children);
        }
      });
    }
    return true;
  };
  const filterRouter = (value: routerType, roules: Array<string>) => {
    return value.filter((itme) => {
      if (itme.children && itme.children.length > 0) {
        itme.children = filterRouter(itme.children, roules);
      }
      if (typeof itme.name === "string" && itme.name) {
        return roules.some((rule) => {
          return (
            itme.name === rule && filterOpen(itme.name as string, slectList)
          );
        });
      }
      return true;
    });
  };
  bindData(["slectList"], "global");
  globalBus.asyncPushState("changeRouter", (value: Array<string>) => {
    if (value.length > 0) {
      const router = filterRouter(roleRouter, value).concat(routerOptions);
      useData(router);
    }
  });
  return useRoutes(data);
};

export default creatProxy(router, ["global"]);
