const errMsg = {
  repetition: "Key name repetition",
  loaded: "Method has not been loaded",
  fnc: "Parameters are not functions",
  separate: "Separate parameter non-string or array string",
  instance: "It must be an instance React object",
  noObj: "The parameter that changes data is not an object type",
  noDataKey: "There is no corresponding key in the data",
  mapListStateNoArr: "The mapListState argument must be an array string",
  mapListStateEmpty: "Array method is empty",
  mapDataNoArr: "The argument to mapData must be an array string",
  mapDataEmpty: "MapData parameters cannot be empty",
  clearMethodsNoArr: "The argument to clearMethods must be an array string",
  clearMethodsEmpty: "clearMethods parameters cannot be empty",
  clearDataKeysNoArr: "The argument to  clearData must be an array string",
  clearDataKeysEmpty: " clearData parameters cannot be empty",
  dataErr: "The initialization data of data must be of object type",
};
export default (name: keyof typeof errMsg, value?: any): void => {
  console.error(
    `${errMsg[name]} \n--${
      value ? "you are " + typeof value + " as " + value : ""
    }`
  );
};
