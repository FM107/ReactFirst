import {
    rStore,
    rBus,
    rStatus
} from './prototype'
import creatProxyFun from './src/creatProxy'
import {
    newStore,
    storeArrType,
    newStatus,
    newBus,
    DataType,
    BusArrType,
    StatusArrType,
    ProxyTypes,
    rBusProtoType
} from './types'

const storeArr: storeArrType = [], busArr: BusArrType = [], statusArr: StatusArrType = []
const creatRStore = <T extends DataType>(options: newStore<T>) => {
    const store = new rStore<T>({ Data: options.data }, options.name)
    store.actions = options.actions(store)
    storeArr.push(store)
    return store
}
const creatRBus = (options: newBus): rBusProtoType => {
    const bus = new rBus(options.name)
    if (options.actions) {
        bus.actions = options.actions(bus)
    }
    busArr.push(bus)
    return bus
}
const creatRStatus = <T extends DataType>(options: newStatus<T>) => {
    const status = new rStatus<T>(options)
    status.actions = options.actions(status)
    statusArr.push(status)
    return status
}
const creatProxy = creatProxyFun(storeArr, busArr, statusArr)
export {
    creatRStore,
    creatProxy,
    creatRBus,
    creatRStatus
}
export type { ProxyTypes }