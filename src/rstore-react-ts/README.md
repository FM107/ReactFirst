# **为您介绍RStore状态总线管理工具 --TypeScript**

了解详细API、反应问题或查看详细版本更新请访问[rstore.top](rstore.top)

## **介绍**

RStore原本是仅仅用于React事件总线的一款工具但RStore绝不应该仅限于事件总线，所以一段时间后RStore成为了一款专门为react打造的状态管理 + 库 + 总线工具，您可以使用状态管理来规范的管理库中的状态，如果追求方便，您也可以直接操作库中的状态，RStore还有两套不同特性的事件总线会在特性中介绍。

## **特性**

1.**同步事件总线**

2.**异步事件总线**

3.**状态管理工具**

4.**全局状态**

5.**选择性状态更新**

6.**全组件支持**

## **安装**

```powershell
npm i rstore-react
```



## **简单的RStore**

```javascript
import { creatRStore } from 'rstore-react' //引入rrstore

const store = { //rstore初始化对象

    offBus: true, //关闭事件总线只用于状态管理

    name: 'testStore', //rstore名称

    data: { //初始化data数据
        name: '小王',
        old: '124',
        nums: 0
    },

    actions(store) { //定义actions方法
        const testFun = () => {
            store.data.name = '小张'
            store.data.name = '12'
        }
        return {
            testFun
        }
    }
}

export default creatRStore(store) //创建一个rstore并且暴露
```

