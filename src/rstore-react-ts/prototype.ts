import {
    Setting,
    rStoreProtoType_type_sum,
    rStoreProtoType,
    DataType,
    rBusProtoType,
    rStatusProtoType
} from './types'
import busAdd from './src/addBus'
import dataAdd from './src/addData'
class rStore<T extends DataType> implements rStoreProtoType<T>{
    readonly storeName: string
    data!: T
    bindData!: rStoreProtoType_type_sum<T>['bindData']
    unBindData!: rStoreProtoType_type_sum<T>['unBindData']
    clearData!: rStoreProtoType_type_sum<T>['clearData']
    changeData!: rStoreProtoType_type_sum<T>['changeData']
    reactive_callback_function_!: rStoreProtoType_type_sum<T>['reactive_callback_function_']
    the_react_instance_array_!: rStoreProtoType_type_sum<T>['the_react_instance_array_']
    actions!: rStoreProtoType_type_sum<T>['actions']
    clearMethods!: rStoreProtoType_type_sum<T>['clearMethods']
    pushState!: rStoreProtoType_type_sum<T>['pushState']
    changeState!: rStoreProtoType_type_sum<T>['changeState']
    asyncPushState!: rStoreProtoType_type_sum<T>['asyncPushState']
    asyncChangeState!: rStoreProtoType_type_sum<T>['asyncChangeState']
    listState!: rStoreProtoType_type_sum<T>['listState']
    function_that_requires_asynchronous_processing!: rStoreProtoType_type_sum<T>['function_that_requires_asynchronous_processing']
    responseCallback!: rStoreProtoType_type_sum<T>['responseCallback']
    constructor(Setting: Setting<T>, storeName: string) {
        const { Data } = Setting
        this.storeName = storeName
        busAdd(this)
        dataAdd<T>(this, Data)
    }
}

class rBus implements rBusProtoType {
    readonly storeName: string
    clearMethods!: rStoreProtoType_type_sum<any>['clearMethods']
    pushState!: rStoreProtoType_type_sum<any>['pushState']
    changeState!: rStoreProtoType_type_sum<any>['changeState']
    asyncPushState!: rStoreProtoType_type_sum<any>['asyncPushState']
    asyncChangeState!: rStoreProtoType_type_sum<any>['asyncChangeState']
    listState!: rStoreProtoType_type_sum<any>['listState']
    function_that_requires_asynchronous_processing!: rStoreProtoType_type_sum<any>['function_that_requires_asynchronous_processing']
    actions?: rStoreProtoType_type_sum<any>['actions']
    constructor(name: string) {
        this.storeName = name
        busAdd(this)
    }
}

class rStatus<T> implements rStatusProtoType<T> {
    readonly storeName: string
    data!: T
    bindData!: rStoreProtoType_type_sum<T>['bindData']
    unBindData!: rStoreProtoType_type_sum<T>['unBindData']
    clearData!: rStoreProtoType_type_sum<T>['clearData']
    changeData!: rStoreProtoType_type_sum<T>['changeData']
    responseCallback!: rStoreProtoType_type_sum<any>['responseCallback']
    reactive_callback_function_!: rStoreProtoType_type_sum<T>['reactive_callback_function_']
    the_react_instance_array_!: rStoreProtoType_type_sum<T>['the_react_instance_array_']
    actions!: rStoreProtoType_type_sum<T>['actions']
    constructor({ name, data }: { name: string, data: T }) {
        this.storeName = name
        dataAdd<T>(this, data)
    }
}

export {
    rStore,
    rBus,
    rStatus
}