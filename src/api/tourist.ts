import request from "../axios/mian";

export const reqGetSomeNews = () =>
  request.get("/news?publishState=2&_expand=category");

export const reqGetNews = (id: number) => request.get(`/news/${id}`);

export const reqStarNews = (id: number, num: number) =>
  request.patch(`/news/${id}`, { star: num });

export const reqWatchNews = (id: number, num: number) =>
  request.patch(`/news/${id}`, { view: num });
