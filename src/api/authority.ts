import request from "../axios/mian";

export const reqAuthorityList = () => request.get("/rights");
export const reqDelAuthority = (id: number) => request.delete(`/rights/${id}`);
export const reqDelDoubleAuth = (id: number) =>
  request.delete(`/children/${id}`);
export const reqChangeAuthority = (id: number, isShow: number) =>
  request.patch(`/rights/${id}`, { pagepermisson: isShow });
export const reqDoubleChangeAuthority = (id: number, isShow: number) =>
  request.patch(`/children/${id}`, { pagepermisson: isShow });
