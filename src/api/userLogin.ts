import request from "../axios/mian";
export const reqLogin = (login: string, password: string) => {
  return request.get(
    `/users?username=${login}&password=${password}&roleState=true&_expand=role`
  );
};
