import request from "../axios/mian";

export const reqUserData = () => request.get("/users");

export const reqRegions = () => request.get("/regions");

export const reqAddUser = (value: any) => request.post("/users", value);

export const reqDelUser = (id: number) => request.delete(`/users/${id}`);

export const reqChangeUser = (id: number, open: boolean) =>
  request.patch(`/users/${id}`, { roleState: open });

export const reqChangeUserData = (id: number, data: any) =>
  request.patch(`/users/${id}`, data);
