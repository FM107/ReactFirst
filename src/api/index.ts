import * as login from "./userLogin";
import * as right from "./light";
import * as auth from "./authority";
import * as rouls from "./rouls";
import * as news from "./newsAdd";
import * as users from "./users";
import * as home from "./home";
import * as tourist from "./tourist";
export default {
  ...login,
  ...right,
  ...auth,
  ...rouls,
  ...users,
  ...news,
  ...home,
  ...tourist,
};
