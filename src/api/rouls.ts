import request from "../axios/mian";

export const reqRoule = () => request.get("/roles");

export const reqSomeRoult = (id: number) => request.get(`/roles?id=${id}`);
export const reqDelRoule = (id: number) => request.delete(`/roles/${id}`);

export const reqChangeRoule = (id: number, value: Array<string>) =>
  request.patch(`/roles/${id}`, { rights: value });
