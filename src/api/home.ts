import request from "../axios/mian";
export const reqViewList = () =>
  request.get(
    "news?publishState=2&_expand=category&_sort=view&_order=desc&_limit=6"
  );

export const reqStartList = () =>
  request.get(
    "news?publishState=2&_expand=category&_sort=star&_order=desc&_limit=6"
  );

export const reqGetSumHome = () =>
  request.get("news?publishState=2&_expand=category");

export const reqUserNews = () =>
  request.get(
    `news?publishState=2&_expand=category&author=${localStorage.getItem(
      "userName"
    )}`
  );
