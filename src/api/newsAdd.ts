import request from "../axios/mian";

export const reqCatrGread = () => request.get("/categories");

export const reqPushNews = (data: any) => request.post("/news", data);

export const reqDrafts = () => request.get(`/news?auditState=0`);

export const reqNewsDetial = (id: number) => request.get(`/news/${id}`);

export const reqChnageDrafts = (id: number, data: any) =>
  request.patch(`/news/${id}`, data);

export const reqDelNews = (id: number) => request.delete(`/news/${id}`);

export const reqReview = () =>
  request.get("/news?auditState_ne=0&publishState_lte=1&_expand=category");

export const reqReviewNews = () =>
  request.get("/news?auditState=1&_expand=category");

export const reqAgreedReview = (id: number) =>
  request.patch(`/news/${id}`, { auditState: 2, publishState: 1 });

export const reqRejectNews = (id: number) =>
  request.patch(`/news/${id}`, { auditState: 3 });

export const reqChangeNewsCategory = (id: number, value: string) =>
  request.patch(`/categories/${id}`, { value, title: value });

export const reqDelNewsCategory = (id: number) =>
  request.delete(`/categories/${id}`);

export const reqPublish = (id: number) =>
  request.patch(`/news/${id}`, {
    publishState: 2,
    publishTime: new Date().getTime(),
  });

export const reqUnpublished = () =>
  request.get("/news?publishState=1&_expand=category");

export const reqReleased = () =>
  request.get("/news?publishState=2&_expand=category");

export const reqTapeOut = () =>
  request.get("/news?publishState=3&_expand=category");

export const reqOutLineNews = (id: number) =>
  request.patch(`/news/${id}`, { publishState: 3 });

export const reqDelNewsOn = (id: number) => request.delete(`/news/${id}`);

export const reqRestLineNews = (id: number) =>
  request.patch(`/news/${id}`, { publishState: 2 });
