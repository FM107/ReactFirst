import { Menu, Button } from "antd";
import { useNavigate, useLocation } from "react-router-dom";
import { ReactElement, useState, useEffect } from "react";
import { creatProxy, ProxyTypes } from "../rstore-react-ts";
import { cloneDeep } from "lodash";
import {
  ApiOutlined,
  MehOutlined,
  PullRequestOutlined,
  ProfileOutlined,
  SecurityScanOutlined,
  ToTopOutlined,
} from "@ant-design/icons";
import request from "../api";
const { reqRight } = request;

type dynamicMenuFunValueType = Array<{
  title: string;
  key: string;
  icons?: ReactElement;
  pagepermisson?: 1;
  children?: dynamicMenuFunValueType;
}>;

const { SubMenu } = Menu;
function Sidebar(props: ProxyTypes) {
  const {
    actions,
    data: { global },
    bindData,
    unBindData,
  } = props.rStatus;

  const { pathname } = useLocation();
  const pathArr = pathname.slice(1).split("/");
  const defultKey = pathArr[2] ? "/" + pathArr[0] + "/" + pathArr[1] : "";
  const navigate = useNavigate();
  const [width, useWidth] = useState<number>(256);
  const [open, useOpen] = useState<boolean>(true);
  useEffect(() => {
    bindData(["slectList", "roules"], "global");
    actions.global.changSlectList();
    return () => {
      unBindData(["slectList", "roules"], "global");
    };
  }, []);
  const filuterRight = (item: dynamicMenuFunValueType) => {
    return item.filter((router) => {
      const { children } = router;
      if (children && children.length > 0) {
        router.children = filuterRight(children);
      }
      return (
        router.pagepermisson &&
        global.roules.length > 0 &&
        (global.roules as [string]).some((item) => item === router.title)
      );
    });
  };
  const dynamicMenu = (
    routerOptions: dynamicMenuFunValueType,
    path = "/main"
  ) => {
    if (routerOptions.length === 0) return null;
    return routerOptions.map((item) => {
      const fullPath = path + "/" + item.key;
      let icons;
      switch (item.key) {
        case "home":
          icons = <ApiOutlined />;
          break;
        case "user-manage":
          icons = <MehOutlined />;
          break;
        case "right-manage":
          icons = <PullRequestOutlined />;
          break;
        case "news-manage":
          icons = <ProfileOutlined />;
          break;
        case "audit-manage":
          icons = <SecurityScanOutlined />;
          break;
        case "publish-manage":
          icons = <ToTopOutlined />;
          break;
      }
      if (icons) {
        item.icons = icons;
      }
      return item.children && item.children.length > 0 ? (
        <SubMenu
          key={fullPath}
          title={item.title}
          icon={item.icons ? item.icons : ""}
        >
          {dynamicMenu(item.children, fullPath)}
        </SubMenu>
      ) : (
        <Menu.Item
          key={fullPath}
          onClick={() => {
            navigate(fullPath);
          }}
          icon={item.icons ? item.icons : ""}
        >
          {item.title}
        </Menu.Item>
      );
    });
  };
  return (
    <div
      style={{
        overflow: "auto",
        height: window.innerHeight,
        paddingBottom: "80px",
      }}
    >
      <div
        style={{
          width: open ? "90%" : "0",
          height: open ? "40px" : "0",
          justifyContent: "center",
          alignItems: "center",
          fontFamily: "Microsoft YaHei",
          margin: "0 auto",
          backgroundColor: "skyblue",
          borderRadius: "5px",
          marginTop: "10px",
          fontSize: open ? "20px" : 0,
          color: open ? "black" : "rgba(0,0,0,0)",
          fontWeight: 700,
          display: "flex",
          transition: "all 0.3s",
          opacity: open ? "100%" : 0,
        }}
      >
        新闻管理系统
      </div>
      <Menu
        style={{ width }}
        mode="inline"
        theme="light"
        inlineCollapsed={!open}
        selectedKeys={[pathname]}
        defaultOpenKeys={[defultKey]}
      >
        {dynamicMenu(filuterRight(cloneDeep(global.slectList)))}
      </Menu>
      <Button
        type={open ? "dashed" : "primary"}
        style={{
          position: "absolute",
          top: window.innerHeight - 50 + "px",
          left: "5px",
          transition: "all 0.3s cubic-bezier(0.22, 0.61, 0.36, 1) ",
          width: width - 10 + "px",
        }}
        onClick={() => {
          useOpen(!open);
          open ? useWidth(70) : useWidth(256);
        }}
      >
        {open ? "收起" : "展开"}
      </Button>
    </div>
  );
}

export default creatProxy(Sidebar, ["global"]);
