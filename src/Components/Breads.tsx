import { useEffect } from "react";
import { creatProxy, ProxyTypes } from "../rstore-react-ts";
import { Menu, Dropdown, Avatar, message } from "antd";
import { UserOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";

const Breads = (props: ProxyTypes) => {
  const navegite = useNavigate();

  const hoverSlect = (
    <Menu>
      <Menu.Item children="更改信息" key="1" style={{ textAlign: "center" }} />
      <Menu.Item
        onClick={() => {
          message.success("退出成功即将返回登录界面");
          localStorage.removeItem("token");
          navegite("/login");
        }}
        children="退出登录"
        key="2"
        style={{ color: "red", textAlign: "center" }}
      />
    </Menu>
  );
  useEffect(() => {
    const {
      rStatus: { bindData, unBindData },
    } = props;

    bindData(["titel", "userName"], "global");
    return () => {
      unBindData(["titel", "userName"], "global");
    };
  }, []);
  const {
    global: { titel, userName },
  } = props.rStatus.data;
  return (
    <div
      style={{
        width: "100%",
        height: "8%",
        backgroundColor: "skyblue",
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
      }}
    >
      <h3
        style={{
          margin: "0 20px",
          fontWeight: 700,
        }}
      >
        {titel}
      </h3>
      <div
        style={{
          margin: "0 20px",
        }}
      >
        欢迎您尊敬的用户<strong>{userName}</strong>
        <Dropdown
          overlay={hoverSlect}
          children={
            <Avatar
              size={40}
              icon={<UserOutlined />}
              style={{ margin: "0 10px" }}
            />
          }
        />
      </div>
    </div>
  );
};

export default creatProxy(Breads, ["global"]);
