import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    proxy: {
      "/saer": {
        target: "http://seerh5.61.com/",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/saer/, ""),
      },
    },
  },
});
